package com.cardpaymentsystem.controllers;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.cardpaymentsystem.models.Card;
import com.cardpaymentsystem.repository.CardRepository;
import com.cardpaymentsystem.services.CardService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(CardController.class)
class CardControllerTest {

	@MockBean
	CardRepository cardRepository;

	@MockBean
	CardService cardService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void testGetcardString() throws Exception {
		Card cardDetails = new Card();

		cardDetails.setCardNumber(1234123411112222L);
		cardDetails.setCardType("Visa");
		cardDetails.setCustomerId(1L);
		cardDetails.setExpirationDate("21-12-2030");
		cardDetails.setEmail("vijay@gmail.com");

		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");
		List<Card> cards = new ArrayList<>();
		cards.add(cardDetails);
		cards.add(cardDetails2);

		when(cardService.getCardByEmail(cardDetails.getEmail())).thenReturn((cards));
		mockMvc.perform(get("/api/card/getcard/{email}", cardDetails.getEmail())).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].cardNumber").value(cardDetails.getCardNumber()))
				.andExpect(jsonPath("$[0].cardType").value(cardDetails.getCardType()))
				.andExpect(jsonPath("$[0].customerId").value(cardDetails.getCustomerId()))
				.andExpect(jsonPath("$[0].expirationDate").value(cardDetails.getExpirationDate()))
				.andExpect(jsonPath("$[0].email").value(cardDetails.getEmail())).andDo(print());

	}

	@Test
	void testGetcardLong() throws Exception {
		Card cardDetails = new Card();

		cardDetails.setCardNumber(1234123411112222L);
		cardDetails.setCardType("Visa");
		cardDetails.setCustomerId(1L);
		cardDetails.setExpirationDate("21-12-2030");
		cardDetails.setEmail("vijay@gmail.com");

		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");
		List<Card> cards = new ArrayList<>();
		cards.add(cardDetails);
		cards.add(cardDetails2);

		when(cardService.getCardByNumber(cardDetails.getCardNumber())).thenReturn((cardDetails));
		mockMvc.perform(get("/api/card/getcarddetails/{cardNumber}", cardDetails.getCardNumber()))
				.andExpect(status().isOk()).andExpect(jsonPath("$.cardNumber").value(cardDetails.getCardNumber()))
				.andExpect(jsonPath("$.cardType").value(cardDetails.getCardType()))
				.andExpect(jsonPath("$.customerId").value(cardDetails.getCustomerId()))
				.andExpect(jsonPath("$.expirationDate").value(cardDetails.getExpirationDate()))
				.andExpect(jsonPath("$.email").value(cardDetails.getEmail())).andDo(print());

	}

	@Test
	void testSaveCard() throws Exception {
		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");

		Mockito.when(cardService.existsByCardNumber(cardDetails2.getCardNumber())).thenReturn(false);

		mockMvc.perform(post("/api/card/savecard").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(cardDetails2))).andExpect(status().isOk()).andDo(print());

	}

	@Test
	void testSaveCard2() throws Exception {
		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");

		Mockito.when(cardService.existsByCardNumber(cardDetails2.getCardNumber())).thenReturn(true);

		mockMvc.perform(post("/api/card/savecard").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(cardDetails2))).andExpect(status().isBadRequest()).andDo(print());

	}

	@Test
	void testUpdateCard() throws Exception {
		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");

		Mockito.when(cardService.existsByCardNumber(cardDetails2.getCardNumber())).thenReturn(true);
		Mockito.when(cardService.existsByEmail(cardDetails2.getEmail())).thenReturn(true);
		Mockito.when(cardService.saveCard(cardDetails2)).thenReturn(cardDetails2);
		mockMvc.perform(put("/api/card/updatecard").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(cardDetails2))).andExpect(status().isOk()).andDo(print());

	}
	
	@Test
	void testUpdateCard2() throws Exception {
		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");

		Mockito.when(cardService.existsByCardNumber(cardDetails2.getCardNumber())).thenReturn(false);
		Mockito.when(cardService.existsByEmail(cardDetails2.getEmail())).thenReturn(true);
		Mockito.when(cardService.saveCard(cardDetails2)).thenReturn(cardDetails2);
		mockMvc.perform(put("/api/card/updatecard").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(cardDetails2))).andExpect(status().isBadRequest()).andDo(print());

	}

	@Test
	void testUpdateCard3() throws Exception {
		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");

		Mockito.when(cardService.existsByCardNumber(cardDetails2.getCardNumber())).thenReturn(true);
		Mockito.when(cardService.existsByEmail(cardDetails2.getEmail())).thenReturn(false);
		Mockito.when(cardService.saveCard(cardDetails2)).thenReturn(cardDetails2);
		mockMvc.perform(put("/api/card/updatecard").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(cardDetails2))).andExpect(status().isBadRequest()).andDo(print());

	}

}

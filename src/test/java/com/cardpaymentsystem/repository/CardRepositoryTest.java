package com.cardpaymentsystem.repository;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.assertj.core.api.Assertions.assertThat;

import com.cardpaymentsystem.models.Card;

@DataJpaTest
class CardRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	CardRepository repository;

	@Test
	void testFindByCardNumber() {
		Card cardDetails = new Card();
		cardDetails.setCardNumber(1234123411112222L);
		cardDetails.setCardType("Visa");
		cardDetails.setCustomerId(1L);
		cardDetails.setExpirationDate("21-12-2030");
		cardDetails.setEmail("vijay@gmail.com");
		entityManager.persist(cardDetails);

		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");
		entityManager.persist(cardDetails2);

		Card payments = repository.findByCardNumber(cardDetails2.getCardNumber());

		assertThat(payments).isEqualTo(cardDetails2);

	}

	@Test
	void testFindByEmail() {
		Card cardDetails = new Card();
		cardDetails.setCardNumber(1234123411112222L);
		cardDetails.setCardType("Visa");
		cardDetails.setCustomerId(1L);
		cardDetails.setExpirationDate("21-12-2030");
		cardDetails.setEmail("vijay@gmail.com");
		entityManager.persist(cardDetails);

		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");
		entityManager.persist(cardDetails2);

		Iterable cards = repository.findByEmail(cardDetails2.getEmail());

		assertThat(cards).hasSize(2).contains(cardDetails, cardDetails2);

	}

	@Test
	void testExistsByCardNumber() {
		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");
		entityManager.persist(cardDetails2);

		Boolean card = repository.existsByCardNumber(cardDetails2.getCardNumber());

		assertThat(card).isEqualTo(true);

	}

	@Test
	void testExistsByEmail() {
		Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");
		entityManager.persist(cardDetails2);

		Boolean card = repository.existsByEmail(cardDetails2.getEmail());

		assertThat(card).isEqualTo(true);

	}

}

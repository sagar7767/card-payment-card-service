package com.cardpaymentsystem.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.cardpaymentsystem.models.Card;
import com.cardpaymentsystem.repository.CardRepository;

class CardServiceTest {

	@Mock
	private CardRepository cardRepository;

	@InjectMocks
	private CardService cardService;

	@BeforeEach
	public void Setup() {
		MockitoAnnotations.initMocks(this);
	}

	Card cardDetails1 = new Card(1234123411112222L, "Amex", 2L, "21-12-2030", "vijay@gmail.com");
	Card cardDetails2 = new Card(1234123411112223L, "Visa", 2L, "21-12-2030", "vijay@gmail.com");

	@Test
	void testSaveCard() {
		cardService.saveCard(cardDetails1);

		verify(cardRepository, times(1)).save(cardDetails1);
	}

	@Test
	void testGetCardByNumber() {

		when(cardRepository.findByCardNumber(cardDetails1.getCardNumber())).thenReturn(cardDetails1);

		Card card = cardService.getCardByNumber(cardDetails1.getCardNumber());

		assertEquals(cardDetails1, card);
		verify(cardRepository, times(1)).findByCardNumber(cardDetails1.getCardNumber());

	}

	@Test
	void testGetCardByEmail() {
		List<Card> list = new ArrayList<Card>();

		list.add(cardDetails1);
		list.add(cardDetails2);

		when(cardRepository.findByEmail(cardDetails1.getEmail())).thenReturn(list);

		List<Card> cardList = cardService.getCardByEmail(cardDetails1.getEmail());

		assertEquals(2, cardList.size());
		verify(cardRepository, times(1)).findByEmail(cardDetails1.getEmail());


	}

	@Test
	void testExistsByCardNumber() {
		when(cardRepository.existsByCardNumber(cardDetails1.getCardNumber())).thenReturn(true);
		Boolean actual = cardService.existsByCardNumber(cardDetails1.getCardNumber());

		assertEquals(true, actual);
		verify(cardRepository, times(1)).existsByCardNumber(cardDetails1.getCardNumber());

	}

	@Test
	void testExistsByEmail() {
		when(cardRepository.existsByEmail(cardDetails1.getEmail())).thenReturn(true);
		Boolean actual = cardService.existsByEmail(cardDetails1.getEmail());

		assertEquals(true, actual);
		verify(cardRepository, times(1)).existsByEmail(cardDetails1.getEmail());

	}

}

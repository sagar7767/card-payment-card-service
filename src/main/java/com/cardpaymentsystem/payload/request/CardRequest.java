package com.cardpaymentsystem.payload.request;

public class CardRequest {

	private long cardNumber;
	private String cardType;
	private Long customerId;
	private String expirationDate;
	private String email;

	public CardRequest() {

	}

	public CardRequest(long cardNumber, String cardType, Long customerId, String expirationDate, String email) {
		
		this.cardNumber = cardNumber;
		this.cardType = cardType;
		this.customerId = customerId;
		this.expirationDate = expirationDate;
		this.email = email;
	}

	public long getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


}

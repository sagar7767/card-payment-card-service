package com.cardpaymentsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CardPaymentCardServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardPaymentCardServiceApplication.class, args);
	}

}

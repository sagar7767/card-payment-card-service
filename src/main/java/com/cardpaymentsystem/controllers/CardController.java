package com.cardpaymentsystem.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cardpaymentsystem.models.Card;
import com.cardpaymentsystem.payload.request.CardRequest;
import com.cardpaymentsystem.payload.response.MessageResponse;
import com.cardpaymentsystem.services.CardService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/card")
public class CardController {

	@Autowired
	private CardService cardService;

	@GetMapping("/getcard/{email}")
	public List<Card> getcard(@PathVariable String email) {

		List<Card> card = cardService.getCardByEmail(email);
		return card;
//		return ResponseEntity.ok(new Card(card.getCardNumber(), card.getCardType(), card.getCustomerId(),
//				card.getExpirationDate(), card.getEmail()));// roles));
	}

	@GetMapping("/getcarddetails/{cardNumber}")
	public Card getcard(@PathVariable long cardNumber) {

		Card card = cardService.getCardByNumber(cardNumber);
		return card;
//		return ResponseEntity.ok(new Card(card.getCardNumber(), card.getCardType(), card.getCustomerId(),
//				card.getExpirationDate(), card.getEmail()));// roles));
	}

	@PostMapping("/savecard")
	public ResponseEntity<?> saveCard(@RequestBody CardRequest cardRequest) {
		if (cardService.existsByCardNumber(cardRequest.getCardNumber())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Card Number is already exist!"));
		}

		// Create new user's account
		Card card = new Card(cardRequest.getCardNumber(), cardRequest.getCardType(), cardRequest.getCustomerId(),
				cardRequest.getExpirationDate(), cardRequest.getEmail());

		cardService.saveCard(card);
		return ResponseEntity.ok(new MessageResponse("Card saved successfully!"));
	}

	@PutMapping("/updatecard")
	public ResponseEntity<?> updateCard(@RequestBody CardRequest cardRequest) {
		if (cardService.existsByCardNumber(cardRequest.getCardNumber())
				&& cardService.existsByEmail(cardRequest.getEmail())) {
			// Update existing user's account
			Card card = new Card(cardRequest.getCardNumber(), cardRequest.getCardType(), cardRequest.getCustomerId(),
					cardRequest.getExpirationDate(), cardRequest.getEmail());

			cardService.saveCard(card);

			return ResponseEntity.ok(new MessageResponse("Card updated successfully!"));
		}

		return ResponseEntity.badRequest().body(new MessageResponse("Error: Card Number not exist!"));

	}

}

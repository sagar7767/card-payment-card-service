package com.cardpaymentsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardpaymentsystem.models.Card;
import com.cardpaymentsystem.repository.CardRepository;

@Service
public class CardService {

	@Autowired
	private CardRepository cardRepository;

	public Card saveCard(Card card) {
		return cardRepository.save(card);
	}

	public Card getCardByNumber(long cardNumber) {
		return cardRepository.findByCardNumber(cardNumber);
	}

	public List<Card> getCardByEmail(String email) {
		return cardRepository.findByEmail(email);
	}

	public Boolean existsByCardNumber(long cardNumber) {
		return cardRepository.existsByCardNumber(cardNumber);
	}

	public Boolean existsByEmail(String email) {
		return cardRepository.existsByEmail(email);
	}
}

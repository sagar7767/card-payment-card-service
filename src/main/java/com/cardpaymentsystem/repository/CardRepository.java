package com.cardpaymentsystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cardpaymentsystem.models.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {

	Card findByCardNumber(long cardNumber);
	
	List<Card> findByEmail(String email);

	Boolean existsByCardNumber(long cardNumber);

	Boolean existsByEmail(String email);
	

}
